#include<stdio.h>
#include<string.h>
#include<stdlib.h>
day01
 DDCCB
int main()//HJ73 计算日期到天数转换
{
    int year, month, date;
    scanf_s("%d %d %d", &year, &month, &date);
    for (int i = 1; i < month; i++)
    {
        if (1 == i || 3 == i || 5 == i || 7 == i || 8 == i || 10 == i || 12 == i)
        {
            date += 31;
        }
        else if (2 == i)
        {
            if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
            {
                date += 29;
            }
            else date += 28;
        }
        else date += 30;
    }
    printf("%d", date);
    return 0;
}
#include<stdio.h>
int main()//JZ17 打印从1到最大的n位数
{
    char s[] = "\\123456\123456\t";
    printf("%d\n", strlen(s));
    return 0;
}
#include<math.h>
int* printNumbers(int n, int* returnSize) {
    // write code here
    *returnSize = pow(10, n) - 1;
    int* arr = (int*)malloc(*returnSize * sizeof(int));//申请动态分配空间
    //void*malloc(size_t size)
    //*returnSize * sizeof(int)实际上是*returnSize × sizeof(int)
    //size_t size 实际上返回的是申请动态分配空间的字节大小
    //假如我们申请一个数组也可以为
    //int *arr = (int *)malloc(sizeof(int))
    for (int i = 0; i < *returnSize; i++)
    {
        arr[i] = i + 1;
    }
    return arr;
}
int f(int n)
{
	static int i = 1;
	if (n >= 5)
		return n;
	n = n + i;
	i++;
	return f(n);
}
int main()
{
	printf("%d", f(1));
	return 0;
}
#include<stdio.h>
#include<math.h>
 day02
 ADCCB
int main()//HJ76 尼科彻斯定理
{
    int a = 0;
    while (~scanf("%d", &a))
    {
        //求首项a1=sn\n-((n-1)*d)/2
        //首项为n*n-n+1
        int d = 0;
        int first_num = a * a - a + 1;
        for (int i = 1; i <= a; i++)
        {
            if (i == a)
            {
                printf("%d", first_num + 2 * (a - 1));
            }
            else   printf("%d+", first_num + 2 * (i - 1));
        }
        putchar('\n');
    }
    return 0;
}
#include<stdio.h>
int main()
{
    int a = 0;
    long sum = 2;
    scanf_s("%d", &a);
    for (int i = 1; i <= a; i++)
    {
        sum = 2 + 3 * (i - 1); 
    }
    printf("%d", sum);
    return 0;
}
 #include <stdio.h>
 int main(void) //HJ100 等差数列
 {
 	int n = 0;
 	while (~scanf("%d", &n)) {
 		//等差求和：sn=n*a1+((n*(n-1)/2)*d
 		printf("%d\n", n * 2 + ((n * (n - 1) / 2) * 3));
 	}
 	return 0;
 }
