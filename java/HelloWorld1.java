public class HelloWorld {
   public static void main1(String[] args) {
        System.out.println("Hello World"); // 输出 Hello World
        System.out.print("不换行");
        System.out.println("自动换行");
        int a = 10;//int为四个字节
        System.out.printf("%d",a);
        System.out.println(a);
        char ch1 = '夏';
        char ch2 = '李';//char类型为两个字节
        System.out.println(ch1);
    }
    public static void main3(String[] args){
      System.out.println(Long.MAX_VALUE);//9223372036854775807
      System.out.println(Long.MIN_VALUE);//-9223372036854775808
      //long 的包装类为 Long
      System.out.println(Byte.MAX_VALUE);//127
      System.out.println(Byte.MIN_VALUE);//-128
      //byte 的包装类为 Byte
      System.out.println(Integer.MAX_VALUE);//2147483647
      System.out.println(Integer.MIN_VALUE);//-2147483648
      //int 的包装类为 Integer
      System.out.println((int)(Character.MAX_VALUE));//65535
      System.out.println((int)(Character.MIN_VALUE));//0
      //因为char这个是以字符类型输出的因此我们应该强制类型转换
      //char 的包装类型为 Characher
      System.out.println(Short.MAX_VALUE);//32767
      System.out.println(Short.MIN_VALUE);//-32768
      //short的包装类为 Short
      System.out.println(Float.MAX_VALUE);//3.4028235E38
      System.out.println(Float.MIN_VALUE);//1.4E-45
      //float的包装类为Float
      System.out.println(Double.MAX_VALUE);//1.7976931348623157E308
      System.out.println(Double.MIN_VALUE);//4.9E-324
      //double的包装类为Double
      // byte a = 128;//故意越界处理
      // System.out.println(a);//当数字越界时，编译器不会编译会报错！！！
      char ch ='a';
      int a = ch + 1;
      System.out.println(ch);//a
      System.out.println((int)ch);//97
      System.out.println(a);//98
      //char会强制类型转换为int类，并且遵循ASCLL码表
      int b = 128;
      System.out.println(b);
      System.out.println("btye类型转换int为:"+(byte)b);//-128
      System.out.println("btye类型转换int为:"+(byte)130);//-126
      System.out.println("btye类型转换int为:"+(byte)131);//-125
    }
    public static void main2(String[] args){
      int 浅 = 100;
      System.out.println(浅);//可以用文字来命名
      int a = 100;
      int b = 200;
      System.out.println("a的初始值为:"+ a);
      System.out.println("a++ 为:"+ (++a));
      System.out.println("b的初始值为:"+ b);
      System.out.println("b--的值为:"+ (--b));
      System.out.println("a + b 的值为:" + (a + b));

    }
    public static void main(String[] args){
      boolean flag1 = true;//布尔类型正确与否
      boolean flag2 = false;//布尔类型的正确与否
      System.out.println(flag1);
       System.out.println(flag2);
       int a = 10;
      int b = 100;
      if(a > b)//布尔表达式
      {
         System.out.println("hellobite!!");
      }
      else{
         System.out.println("hellobanni!!!!");
      }
      int c = 20;
      while(c < 30)
      {
         System.out.println(c++);

      }
      int [] numbers = {10,20,30,40,50};
      for(int i = 0; i < numbers.length; i++)
      {
         System.out.println(numbers[i]);
      }
      

    }
}