//#include<stdio.h>
//struct Class
//{
//	char id[20];
//	char name[100];
//	int age;
//	char sex[100];
//};
//void print(struct Class* pc)//输入结果需要有&符号
//{
//	printf("%s %s %d %s\n", pc->id, pc->name, pc->age, pc->sex);//取地址来输出
//}//
//int main()
//{   struct Class s1 = { "202007010723","tim",16,"男" };
//    struct Class s2 = { "20207010233","banni",17,"男" };
//	printf("%s %s %d %s\n", s1.id, s1.name, s1.age, s1.sex);
//	print(&s2);
//	return 0;
//}//结构体的运用
//#include<stdio.h>//指针的演示
//int main()
//{
//	int a = 10;
//	int* pa = &a;//吧a的地址存在*pa中（*pa的内存存的就是a的地址）
//	*pa = 20;//(将a地址的值改变）
//	printf("%d", a);//输出为20
//	printf("%p", &a);//&取地址符，取a的地址
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char ch[20];
//	scanf_s("%s", ch[20]);//字符串不需要&来输入
//	printf("%s", ch[20]);
//	return 0;
//}
//#define MAX(x,y,z) ((x>y&&x>z)?(x):(y>z)?(y):(z))//define 宏
//#include<stdio.h>
//int main()//测试MAX实用性
//{
//	int a = 1;
//	int b = 2;
//	int c = 3;
//	printf("%d", MAX(a, b, c));
//	return 0;
//}
// 
// 
//#include<stdio.h>
//int main()
//{
//	int a, b, c;
//	float e;
//	scanf_s("%d %d %d", &a, &b, &c);
//	e = (float)MAX(a + b, b, c) / (MAX(a, b + c, c) + MAX(a, b, b + c));//3/3+5
//	printf("%.2f", e);
//	return 0;
//}
//从键盘输入5个人的身高（米），求他们的平均身高（米）。
//#include<stdio.h>
//int main()
//{
//	float a, b, c, d, e;
//	scanf_s("%f %f %f %f %f", &a, &b, &c, &d, &e);
//	printf("%.2f", (a + b + c + d + e) / 5);
//	return 0;
//}
//输入一个班级5个学生各5科成绩，输出5个学生各5科成绩及总分。
//#include<stdio.h>
//int main()
//{
//	float a, b, c, d, e;
//	while (scanf_s("%f %f %f %f %f", &a, &b, &c, &d, &e) != EOF)
//	{
//		printf("%f %f %f %f %f %.1f\n",a,b,c,d,e,(a + b + c + d + e) / 5);
//	}
//		return 0;
//}
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“*”组成的线段图案。
//#include<stdio.h>
//int main()
//{
//	int a,i;
//	while (scanf_s("%d", &a) != EOF)
//	{
//		for (i = 0; i < a; i++)
//		{
//			printf("*");
//         }
//		printf("\n");
//	}
//	return 0;
//}
