//查二进制序列里面的1用x&(1<<n)
//设某一项为1   x|(1<<n)
//设某一项为1   x~&(1<<n)
//KS33 寻找奇数
//一个数，即在序列中唯一出现奇数次的数值。
#include<stdio.h>
int main()
{
    long long n = 0;
    long long max = 0;
    scanf("%d", &n);
    long long x = 0;
    while (~scanf("%d", &x))
    {
        max ^= x;
    }
    printf("%ld", max);
    return 0;
}
//NC107 寻找峰值
//峰值就是寻找最大值
int findPeakElement(int* nums, int numsLen)
{
    int left = 0;
    int right = numsLen - 1;
    while (left < right)
    {
        int mid = (left + right) >> 1;
        if (nums[mid] < nums[mid + 1])
        {
            left = mid + 1;
        }
        else right = mid;
    }
    return right;
}
//HJ46 截取字符串
#include<stdio.h>
#include<string.h>
int main()
{
    char ch[100] = { 0 };
    int k = 0;
    while (~scanf("%s %d", ch, &k))
    {
        ch[k] = '\0';
        printf("%s\n", ch);

    }
    return 0;
}
//WY49 数对
long n = 0, k = 0;
while (~scanf("%ld %ld", &n, &k))
{
    if (k == 0)
    {
        printf("%ld\n", n * n);
        continue;
    }
    long count = 0;
    for (long y = k + 1; y <= n; y++)
    {
        long ret = n % y < k ? 0 : n % y - (k - 1);
        count += (y - k) * (n / y) + (ret);
    }
    printf("%ld\n", count);
}