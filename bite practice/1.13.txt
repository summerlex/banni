//int main()//
//{
	//int x = 0;
	//scanf_s("%d", &x);
//===============================switch的运用===============================
//	switch (x) 
//	{
//	case 1:printf("1");
//	case 2:printf("2");
//		break;//当没有break的时候输入1输出为12，输入2输出2，没有break，case会往下走
//	case 3:printf("3");
//		break;//输入3的时候就会输出3，因为有break只会走case3的1语句
//	default: printf("5");//default的意思是假如没有case匹配的值那就返回这个值
////假如我们输入1999，不在范围内就会返回5 
//	case 100:printf("嵌套式switch的演示\n");
//		int y = 0;//当输入100时进入第二个switch
//		scanf_s("%d", &y);
//		switch (y)//
//		{
//		case 7:printf("7");
//		case 8:printf("8");//输入100然后7打印出78
//			break;
//		case 9:printf("9");
//			break;
//		default:printf("退出嵌套式");
//		}
//	case 10000:char ch = 0;
//		ch = getchar();//输入字符
//			switch (ch)
//			{
//			case 'A':printf("a");//字符也一样只不过字符需要用''表示
//			case 'B':printf("b");
//				break;
//			case'C':printf("C");
//			}
//	}
//=================================无限循环输出============================//
	//for (;;)
	//{
	//	printf("c++");
	//}//这将会无限循环c++
//==================================数组地址输出============================//
//	int brr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//test(brr);//测试指针函数
//	int* p;
//	p=brr;//brr实际上是brr数组的地址
//	for (int i = 0; i < 5; i++)//使用指针的数组值
//	{
//		printf("%d\n", *(p + i));//*(p+i)也可以读取数组里面的元素
//	}//前提是p存的是brr的地址
//	for (int i = 0; i < 5; i++)//使用 balance 作为地址的数组值
//	{
//		printf("%d\n", *(brr + i));//同理*（brr+i）也可以读取数组的元素
//	}//brr也就是数组前缀其实就是数组的地址
//	return 0;
//	//srand((unsigned)time(NULL))是初始化随机函数种子
//}
