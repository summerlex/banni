//int main()
//{
////数组名是什么呢？
////数组名通常来说是数组首元素的地址
////但是有2个例外：
////1. sizeof(数组名)，这里的数组名表示整个数组，计算的是整个数组的大小
////2. &数组名，这里的数组名表示整个数组，取出的是整个数组的地址
//sizeof是一个操作符
//sizeof 计算的是对象所占内存的大小-单位是字节,size_t
//不在乎内存中存放的是什么，只在乎内存大小
// =================================一维数组==========================================================
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));//a数组名代表整个数组，计算的是整个数组的大小 因此输出为 : 4 * 4 = 16 
//	printf("%d\n", sizeof(a + 0));// (a + 0)没有解引用为地址，因此是个指针，是第一个元素的地址， 因此大小为 : 4(x64) | 8(x86) 
//	printf("%d\n", sizeof(*a));// *a 也可以转变为*(a + 0),对数组第一个第一个元素进行解引用 因此大小为: 4
//	printf("%d\n", sizeof(a + 1));//(a + 1)和(a + 0)一样，是数组第二个元素的地址 ,因此大小为: 4(x64) | 8(x86)
//	printf("%d\n", sizeof(a[1]));// a[1] == (a + 1)，同上 大小为: 4(x64) | 8(x86)
//	printf("%d\n", sizeof(&a));// &a去出的是整个数组的地址  &a == int(*p)[4] 是一个数组指针 , 因此大小为: 4(x64) | 8(x86)
//	printf("%d\n", sizeof(*&a));// *(&a)解引用整个数组的地址,得到的是一个数组的大小 因此大小为: 4 * 4 = 16  
//	printf("%d\n", sizeof(&a + 1));//(&a + 1) &a为整个a数组的地址 &a + 1就是a数组之后的数字的地址 也就是说他就是一个指针 因此大小为: 4(x64) | 8(x86)
//	printf("%d\n", sizeof(&a[0]));//&a[0] == &(a + 0) 数组第一个数的地址 因此大小为: 4(x64) | 8(x86)
//	printf("%d\n", sizeof(&a[0] + 1));//&a[0] + 1 == &a[1] 数组第二个数的地址 因此大小为: 4(x64) | 8(x86)
//	return 0;
//}
int main()
{
	//字符数组
	//char arr[] = { 'a','b','c','d','e','f' };
	//printf("%d\n", sizeof(arr));//arr数组名代表整个数组，计算的是整个数组的大小 因此输出为 : 1 * 6
	//printf("%d\n", sizeof(arr + 0));//(att + 0)没有解引用为地址，因此是个指针，是第一个元素的地址， 因此大小为 : 4(x64) | 8(x86)
	//printf("%d\n", sizeof(*arr));//*arr 也可以转变为* (arr + 0), 对数组第一个第一个元素进行解引用 因此大小为 : 1
	//printf("%d\n", sizeof(arr[1]));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(&arr));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(&arr + 1));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(&arr[0] + 1));//4(x64) | 8(x86)
	////strlen遇到\0才会停止
	//printf("%llu\n", strlen(arr));//随机值
	//printf("%llu\n", strlen(arr + 0));//随机值
	//printf("%llu\n", strlen(*arr));//(*arr)对第一个元素地址进行解引用，但是strlen函数只能传地址因此这条程序err
	//printf("%llu\n", strlen(arr[1]));//*(arr + 1) == arr[1] 对第二个元素地址进行解引用，但是strlen函数只能传地址因此这条程序err
	//printf("%llu\n", strlen(&arr));//随机值
	//printf("%llu\n", strlen(&arr + 1));////随机值
	//printf("%llu\n", strlen(&arr[0] + 1));////随机值
	//大多数为随机值的原因是因为他没有\0，因此得不断寻找直到找到嘞个才停止
	//那现在我们加入一个\0 看看结果怎么样
	//char arr[] = "abcdef";
	////a b c d e f \0
	//printf("%d\n", sizeof(arr));//7
	//printf("%d\n", sizeof(arr + 0));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(*arr));//1
	//printf("%d\n", sizeof(arr[1]));//1
	//printf("%d\n", sizeof(&arr));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(&arr[0] + 1));//4(x64) | 8(x86)
	//printf("%d\n", sizeof(&arr + 1));//4(x64) | 8(x86)
	//printf("%d\n", strlen(arr));//6 从'a'开始寻找\0
	//printf("%d\n", strlen(arr + 0));//6 从'a'开始寻找\0
	//printf("%d\n", strlen(*arr));//err
	//printf("%d\n", strlen(arr[1]));//err
	//printf("%d\n", strlen(&arr));//6 从'a'开始寻找\0
	//printf("%d\n", strlen(&arr + 1));//随机值
	//printf("%d\n", strlen(&arr[0] + 1));//5 从'b'开始寻找\0
	//return 0;
	//char* p = "abcdef";
	//*p存的是 'a'的地址
	//printf("%d\n", strlen(p));//6
	//printf("%d\n", strlen(p + 1));//5 从b的位置开始向后数字符
	////printf("%d\n", strlen(*p));  //err
	////printf("%d\n", strlen(p[0]));//err
	//printf("%d\n", strlen(&p));//随机值
	//printf("%d\n", strlen(&p + 1));//随机值
	//printf("%d\n", strlen(&p[0] + 1));//5  从b的位置开始向后数字符

	//printf("%d\n", sizeof(p));//4/8  p是指针变量，计算的是指针变量的大小
	//printf("%d\n", sizeof(p + 1));//4/8 p+1是'b'的地址
	//printf("%d\n", sizeof(*p)); //1  - *p 其实就是'a'
	//printf("%d\n", sizeof(p[0]));//1 - p[0]-> *(p+0)-> *p
	//printf("%d\n", sizeof(&p));//4/8 &p 是指针变量p在内存中的地址
	//printf("%d\n", sizeof(&p + 1));//4/8 - &p+1是跳过p之后的地址
	//printf("%d\n", sizeof(&p[0] + 1));//4/8 &p[0]是‘a’的地址，&p[0]+1就是b的地址
	//二维数组
	int a[3][4] = { 0 };
	//可以吧这个数组理解成 a[3][4] = {{},{},{},{}}
	printf("%d\n", sizeof(a));//整个数组的大小 输出为: 48
	printf("%d\n", sizeof(a[0][0]));//a[0][0]为一个数 输出为:sizeof(int) = 4
	printf("%d\n", sizeof(a[0]));//*(a + 0)解引用第一行的地址 也就是说输出为: 1 * 4 * 4 = 16
	printf("%d\n", sizeof(a[0] + 1));//a[0]就是数组首元素的地址，就是第一行第一个元素的地址，a[0]+1就是第一行第二个元素的地址,输出为:4(x64) | 8(x86)
	printf("%d\n", sizeof(*(a[0] + 1)));// *(*(a + 0) + 1)解引用为第二行第一个元素 也就是输出为 : 4
	printf("%d\n", sizeof(a + 1));//(a + 1)第二行的地址 ，输出为：4(x64) | 8(x86)
	printf("%d\n", sizeof(*(a + 1)));//*(a + 1)解引用第二行的地址 ，输出为: 16
	printf("%d\n", sizeof(&a[0] + 1));//&a[0]取出的是第一行的的地址 + 1 就是取出第二行的地址,输出为: 4(x64) | 8(x86)
	printf("%d\n", sizeof(*(&a[0] + 1)));// *(&a[0] + 1) 对第二行解引用 输出为: 16
	printf("%d\n", sizeof(*a));//*a - > *(a+0) -> a[0] 输出为: 16
	return 0;

}