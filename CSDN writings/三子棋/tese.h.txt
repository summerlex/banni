#define ROW 3
#define COL 3
//宏棋盘大小
#include<stdio.h>
#include<stdlib.h>//srand函数控制rand的随机性
#include<time.h>
//引入的函数
void intiboard(char arr[ROW][COL], int row, int col);
void displayboard(char arr[ROW][COL], int row, int col);
void playermove(char arr[][COL], int row, int col);
void AImove(char arr[][COL], int row, int col);
int Full(char arr[][COL], int row, int col);
char Winner(char arr[][COL], int row, int col);